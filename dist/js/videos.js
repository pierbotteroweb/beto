    var botao = document.getElementsByClassName("botoes--botao"),
    videos = document.getElementsByClassName("videos__wrapperVideos")[0],
    infoVideos = document.getElementsByClassName("infovideos__wrapperInfovideos")[0],
    janela = document.getElementsByClassName("janela--imagem")[0];

    function anima(x){
        var posbgjanela = '0px '+(-186*(x-1))+'px';
        var posvideos = (-376*(x-1))+'px';
        var posinfoVideos = (-200*(x-1))+'px';
        for (i=1;i<=botao.length;i++){
            botao[i-1].style.backgroundColor = 'lightgrey';
        }
        botao[x-1].style.backgroundColor = 'lightblue';
        videos.style.top = posvideos;
        infoVideos.style.top = posinfoVideos;
        janela.style.backgroundPosition = posbgjanela;
    }

    for (g=1;g<=botao.length;g++){
        let volta = g;
        botao[g-1].addEventListener("click",function(){
            anima(volta)
        });
    }