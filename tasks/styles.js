
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var cssimport = require('gulp-cssimport');
var sassVariables = require('gulp-sass-variables');

gulp.task('browser',function(){
	browserSync.init({
    watch: true,
		server:"./dist"
	});
});

gulp.task('importa',['sass'], function(){
	return gulp.src('src/csstemp/style.css')
	.pipe(cssimport())
	.pipe(gulp.dest('dist'))
})



gulp.task('sass',function(){
	return gulp.src('src/*.scss')
	.pipe(sass())
	.pipe(autoprefixer({
	    browsers: ['last 2 versions'],
	    cascade: false
	}))
	.pipe(gulp.dest('src/csstemp'))
});