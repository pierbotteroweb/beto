var gulp = require('gulp'),
	rename = require('gulp-rename'),
	watch = require('gulp-watch'),
	injectStr = require('gulp-inject-string');

var getFooter = "<?php wp_footer(); ?>",
	favicoLink = '<?php echo get_template_directory_uri(); ?>',
	cssLink = '<?php echo get_stylesheet_uri();?>';


href="/imagens/beto.ico"

function wrapAndreplace(templateName){
	gulp.src('src/_'+templateName+'.html')
	.pipe(injectStr.wrap('<?php /* Template Name: '+templateName+
		' */ ?><?php get_header(); ?>','<?php get_footer(); ?>'))
	.pipe(injectStr.replace('<!--getFooter-->',getFooter))
	.pipe(injectStr.replace('.html" class="parag"','" class="parag"'))
	.pipe(injectStr.replace('/js/clipes.js','<?php echo get_template_directory_uri(); ?>/js/clipes.js'))
	.pipe(rename(templateName+'.php'))
	.pipe(gulp.dest('wordpress'))	
}


function headerBodyreplace(bodyPart){
	gulp.src('src/_'+bodyPart+'.html')
	.pipe(injectStr.replace('<!--bodyCtt',""))
	.pipe(injectStr.replace('bodyCtt-->',""))
	.pipe(injectStr.replace('<li><a href="#" class="link-header" style="color:#f00"></a></li>',""))
	.pipe(injectStr.replace('style.css',cssLink))
	.pipe(injectStr.replace('href="/"','href="home"'))
	.pipe(injectStr.replace('contato.html',"contato"))
	.pipe(injectStr.replace('<body><!-- -->',""))
	.pipe(injectStr.replace('href="/imagens/beto.ico"',
		'href="'+favicoLink+'/imagens/beto.ico"'))
	.pipe(rename(bodyPart+'.php'))
	.pipe(gulp.dest('wordpress'))	
}

var paginas = ['index',
				'cenarios',
				'clipes',
				'comerciais',
				'internacionais',
				'longas',
				'televisao',
				'contato'];

var bodyParts = ['header',
				'footer',];

gulp.task('injectStr',function(){
	for (var pg of paginas){
		wrapAndreplace(pg);
	}
	for (var bp of bodyParts){
		headerBodyreplace(bp);
	}
});