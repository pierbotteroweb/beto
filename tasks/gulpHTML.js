
var gulp = require('gulp');
var watch = require('gulp-watch');
var concat = require('gulp-concat');

var paginas = ['index',
				'cenarios',
				'clipes',
				'comerciais',
				'internacionais',
				'longas',
				'televisao',
				'contato'];

var paginasX = ['indexX',
				'comerciaisX',
				'clipesX',
				'longasX',
				'internacionaisX',
				'cenariosX',
				'televisaoX',
				'contatoX'];

gulp.task('montagem',function(){
	function montaPagina(modulo,final){
		return gulp.src(['src/_header.html','src/'+modulo,'src/_footer.html'])
		.pipe(concat(final))
		.pipe(gulp.dest('dist'))
	}

	for (var pag of paginas){
		montaPagina('_'+pag+'.html',pag+'.html');
	}
});

gulp.task('montagemX',function(){
	function montaPaginaX(modulo,final){
		return gulp.src(['src/_headerX.html','src/'+modulo,'src/_footerX.html'])
		.pipe(concat(final))
		.pipe(gulp.dest('dist'))
	}

	for (var pag of paginasX){
		montaPaginaX('_'+pag+'.html',pag+'.html');
	}
});

gulp.task('default',['injectStr','montagem','montagemX','browser'],function(){
	gulp.watch("./src/*.scss",['importa','sass']);
	gulp.watch('./src/*.html',['injectStr','montagem','montagemX'])
});
