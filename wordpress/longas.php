<?php /* Template Name: longas */ ?><?php get_header(); ?>
<div class="row">
  <div class="wrap">
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__videos">
        <div class="videos">
            <div class="videos__wrapperVideos">
            <div class="videos--item">
                <iframe src="https://www.youtube.com/embed/VkK4vS-E-GI?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>BOCA</p>
            </div>
            <div class="videos--item">
                <iframe src="https://www.youtube.com/embed/BtLlREOgysI?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>O OUTRO LADO DO PARAÍSO</p>
            </div>
            </div>
        </div></div>
    <div class="janela janela__apenasDesktop janela__botoes">
        <div class="botoes">
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
        </div>
    </div>
    <div class="janela janela__apenasDesktop janela--imagem janela__imgLongas"></div> 
    <div class="janela janela__apenasDesktop janela__invisivel">

    </div>
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__apenasDesktop janela__infovideos">
        <div class="infovideos">
            <div class="infovideos__wrapperInfovideos">
                <div class="infovideos--legenda infovideos--legenda__longas">
                    <p>BOCA</p>
                    <p style="font-weight:bold">Ano 2010</p>
                    <p>Premio Melhor direção de Arte no Festival do Audiovisual - Cine PE de 2012</p>               
                </div>
                <div class="infovideos--legenda infovideos--legenda__longas">
                    <p>O Outro Lado do Paraíso</p>
                    <p style="font-weight:bold">Ano 2014</p>
                    <p>Troféu Câmara Legislativa do Distrito Federal - Júri Oficial por Melhor direção de Arte no 48º Festival de Brasília do
                    Cinema Brasileiro</p>               
                </div>
            </div>
        </div>
    </div> 
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__apenasDesktop janela__invisivel"></div>    
  </div>
</div>
<!--------------------MAIN FIM-------------------------------->


<?php wp_footer(); ?>
</body>


<script type="text/javascript">
        var botao = document.getElementsByClassName("botoes--botao"),
    videos = document.getElementsByClassName("videos__wrapperVideos")[0],
    infoVideos = document.getElementsByClassName("infovideos__wrapperInfovideos")[0],
    janela = document.getElementsByClassName("janela--imagem")[0];

    function anima(x){
        var posbgjanela = '0px '+(-186*(x-1))+'px';
        var posvideos = (-376*(x-1))+'px';
        var posinfoVideos = (-200*(x-1))+'px';
        for (i=1;i<=botao.length;i++){
            botao[i-1].style.backgroundColor = 'lightgrey';
        }
        botao[x-1].style.backgroundColor = 'lightblue';
        videos.style.top = posvideos;
        infoVideos.style.top = posinfoVideos;
        janela.style.backgroundPosition = posbgjanela;
    }

    for (g=1;g<=botao.length;g++){
        let volta = g;
        botao[g-1].addEventListener("click",function(){
            anima(volta)
        });
    }
</script>
<?php get_footer(); ?>