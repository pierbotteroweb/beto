<?php /* Template Name: cenarios */ ?><?php get_header(); ?><!--------------------MAIN INICIO-------------------------------->



<div class="row">
  <div class="wrap">
    <div class="janela janela__cenarios"></div> 
    <div class="janela janela__cenarios"></div>
    <div class="janela janela__cenarios  janela__botoes">
        <div class="botoes botoes__apenasDesktop">
            <div class="botoes--botao botoes--cenario"></div>
            <div class="botoes--botao botoes--cenario"></div>
            <div class="botoes--botao botoes--cenario"></div>
        </div>
    </div>
    <div class="janela janela__cenarios"></div> 
    <div class="janela janela__cenarios"></div>
    <div class="janela janela__cenarios"></div>
    <div class="janela janela__cenarios"></div> 
    <div class="janela janela__cenarios"></div>
    <div class="janela janela__cenarios"></div>    
  </div>


  <div class="wrap wrap__apenasMobile">
    <div class="janela janela__bgPos-217 janela__cenarios"></div> 
    <div class="janela janela__bgPos-217 janela__cenarios"></div>
    <div class="janela janela__bgPos-217 janela__cenarios"></div>
    <div class="janela janela__bgPos-217 janela__cenarios"></div> 
    <div class="janela janela__bgPos-217 janela__cenarios"></div>
    <div class="janela janela__bgPos-217 janela__cenarios"></div>
    <div class="janela janela__bgPos-217 janela__cenarios"></div> 
    <div class="janela janela__bgPos-217 janela__cenarios"></div>
    <div class="janela janela__bgPos-217 janela__cenarios"></div>    
  </div>


  <div class="wrap wrap__apenasMobile">
    <div class="janela janela__bgPos-434 janela__cenarios"></div> 
    <div class="janela janela__bgPos-434 janela__cenarios"></div>
    <div class="janela janela__bgPos-434 janela__cenarios"></div>
    <div class="janela janela__bgPos-434 janela__cenarios"></div> 
    <div class="janela janela__bgPos-434 janela__cenarios"></div>
    <div class="janela janela__bgPos-434 janela__cenarios"></div>
    <div class="janela janela__bgPos-434 janela__cenarios"></div> 
    <div class="janela janela__bgPos-434 janela__cenarios"></div>
    <div class="janela janela__bgPos-434 janela__cenarios"></div>    
  </div>
</div>


<!--------------------MAIN FIM-------------------------------->


</body>


<script type="text/javascript">
    
var botaoCenarios = document.getElementsByClassName("botoes--cenario"),
janelaCenarios = document.getElementsByClassName("janela__cenarios");

function animaCenarios(x){
    var posbgjanelaCenarios = '0px '+(-217*(x-1))+'px';
    for (i=1;i<=botaoCenarios.length;i++){
        botaoCenarios[i-1].style.backgroundColor = 'lightgrey';
    }
    for (k=1;k<=janelaCenarios.length;k++){
        janelaCenarios[k-1].style.backgroundPosition = posbgjanelaCenarios;
    }
    botaoCenarios[x-1].style.backgroundColor = 'lightblue';
}


for (g=1;g<=botaoCenarios.length;g++){
    let volta = g;
    botaoCenarios[g-1].addEventListener("click",function(){
          animaCenarios(volta)
      });
}
</script>











<?php get_footer(); ?>