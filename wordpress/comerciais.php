<?php /* Template Name: comerciais */ ?><?php get_header(); ?>
<div class="row">
  <div class="wrap">
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__videos">
        <div class="videos">
            <div class="videos__wrapperVideos">
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/hgXFMUpxy7w?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>CAFÉ PELÉ  -  LUX FILMES</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/P_c9CXZZ8zs?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>GM -   SENTIMENTAL FILMES</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/WjPw76MLieA?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>OI - CIA DE CINE</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/bRqsgdqywGI?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>OUTBACK - LUX FILMES</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/G9IFIlO39I4?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>ITAÚ - O2 FILMES</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/iVt9Fjm0o4I?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>UNILEVER - BOSSA NOVA FILMES</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/SOaFxawwso8?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>GAROTO - CRASH OF RHINOS</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/HLgG7hkYR5U?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>MOLICO - BOSSA NOVA FILMS</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/FI1GwsCU6Cg?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>SKOL LOVE STORY - SENTIMENTAL FILMES</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://player.vimeo.com/video/194513673" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                <p>CAIXA | MEGA DA VIDARA - PILOTO</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/TyQgg6MZj8U?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>CAIXA | MEGA DA VIDARA AEROPORTO - PILOTO</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/72xeMgc7-tg?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>CLARO | ESFERAS - PILOTO</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/gTUqfA8yKHc?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>WALMART | BLACK FRIDAY - PILOTO</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://player.vimeo.com/video/219908360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                <p>SPORTV | NOSSO FUTEBOL - ESPECIALISTAS 60”</p>

                </div>
                <div class="videos--item">
                    <iframe src="https://player.vimeo.com/video/225137500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                <p>SPORTV | TIPOS DE TORCEDOR</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/Zv8XtE7Kzzg?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>SANTANDER | DIA DAS MÃES - PILOTO</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/ZO7koJLGhNs?showinfo=0" frameborder="0" allowfullscreen></iframe>
                <p>SPORTV | MOCKUMENTARY - PILOTO</p>
                </div>
            </div>
        </div></div>
    <div class="janela janela__apenasDesktop janela__botoes">
        <div class="botoes">
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
        </div>
        <div class="botoes botoes__coluna2">
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
        </div>
    </div>
    <div class="janela janela__apenasDesktop janela--imagem janela__imgComerciais"></div> 
    <div class="janela janela__apenasDesktop janela__invisivel">

    </div>
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__apenasDesktop janela__infovideos">
        <div class="infovideos">
            <div class="infovideos__wrapperInfovideos">
                <div class="infovideos--legenda">
                    <p>CAFÉ PELÉ</p>
                    <p>LUX FILMES</p>
                </div>
                <div class="infovideos--legenda">
                    <p>GM</p>
                    <p>SENTIMENTAL FILMES</p>
                </div>
                <div class="infovideos--legenda">
                    <p>OI</p>
                    <p>CIA DE CINE</p>
                </div>
                    <div class="infovideos--legenda">
                    <p>OUTBACK</p>
                    <p>LUX FILMES</p>
                </div>
                <div class="infovideos--legenda">
                    <p>ITAÚ</p>
                    <p>O2 FILMES</p>
                </div>
                <div class="infovideos--legenda">
                    <p>UNILEVER</p>
                    <p>BOSSA NOVA FILMES</p>
                </div>
                <div class="infovideos--legenda">
                    <p>GAROTO</p>
                    <p>CRASH OF RHINOS</p>
                </div>
                <div class="infovideos--legenda">
                    <p>MOLICO</p>
                    <p>BOSSA NOVA FILMS</p>
                </div>
                <div class="infovideos--legenda">
                    <p>SKOL LOVE STORY</p>
                    <p>SENTIMENTAL FILMES</p>
                </div>
                <div class="infovideos--legenda">
                    <p>CAIXA | MEGA DA VIRADA</p>
                    <p>PILOTO</p>
                </div>
                <div class="infovideos--legenda">
                    <p>CAIXA | AEROPORTO</p>
                    <p>PILOTO</p>
                </div>
                <div class="infovideos--legenda">
                    <p>CLARO | ESFERAS</p>
                    <p>PILOTO</p>
                </div>
                <div class="infovideos--legenda">
                    <p>WALLMART | BLACK FRIDAY</p>
                    <p>PILOTO</p>
                </div>
                <div class="infovideos--legenda">
                    <p>SPORTV | TIPOS DE TORCEDOR</p>
                    <p>PILOTO</p>
                </div>
                <div class="infovideos--legenda">
                    <p>NOSSO FUTEBOL - ESPECIALISTAS 60”</p>
                    <p>PILOTO</p>
                </div>
                <div class="infovideos--legenda">
                    <p>SANTANDER | DIA DAS MÃES</p>
                    <p>PILOTO</p>
                </div>
                <div class="infovideos--legenda">
                    <p>SPORTV | MOCKUMENTARY</p>
                    <p>PILOTO</p>
                </div>
            </div>
        </div>
    </div> 
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__apenasDesktop janela__invisivel"></div>    
  </div>
</div>
<!--------------------MAIN FIM-------------------------------->


<?php wp_footer(); ?>
</body>


<script type="text/javascript">
        var botao = document.getElementsByClassName("botoes--botao"),
    videos = document.getElementsByClassName("videos__wrapperVideos")[0],
    infoVideos = document.getElementsByClassName("infovideos__wrapperInfovideos")[0],
    janela = document.getElementsByClassName("janela--imagem")[0];

    function anima(x){
        var posbgjanela = '0px '+(-186*(x-1))+'px';
        var posvideos = (-376*(x-1))+'px';
        var posinfoVideos = (-200*(x-1))+'px';
        for (i=1;i<=botao.length;i++){
            botao[i-1].style.backgroundColor = 'lightgrey';
        }
        botao[x-1].style.backgroundColor = 'lightblue';
        videos.style.top = posvideos;
        infoVideos.style.top = posinfoVideos;
        janela.style.backgroundPosition = posbgjanela;
    }

    for (g=1;g<=botao.length;g++){
        let volta = g;
        botao[g-1].addEventListener("click",function(){
            anima(volta)
        });
    }
</script>
<?php get_footer(); ?>