<?php /* Template Name: clipes */ ?><?php get_header(); ?>
<div class="row">
  <div class="wrap">
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__videos">
        <div class="videos">
            <div class="videos__wrapperVideos">
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/kPrWzsrpHis?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>PATO FU - Eu</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/jJhnige5jDQ?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>LENINE - Paciência</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/mq3td6etR5A?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>LENINE - Dois olhos negros</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/d_jQcYmUbE8?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>LENINE - O homem dos olhos de Raio X</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/zZbsQpfFsB0?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>PATO FU - Antes que seja tarde</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/ljbYJoU3guY?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>SANDY, JUNIOR - Imortal</p>
                </div>
            </div>
        </div></div>
    <div class="janela janela__apenasDesktop janela__botoes">
        <div class="botoes">
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
        </div>
    </div>
    <div class="janela janela__apenasDesktop janela--imagem janela__imgClipes"></div> 
    <div class="janela janela__apenasDesktop janela__invisivel">

    </div>
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__apenasDesktop janela__infovideos">
        <div class="infovideos">
            <div class="infovideos__wrapperInfovideos">
                <div class="infovideos--legenda">
                    <p>PATO FU</p>
                    <p>Eu</p>
                </div>
                <div class="infovideos--legenda">
                    <p>LENINE</p>
                    <p>Paciência</p>
                </div>
                <div class="infovideos--legenda">
                    <p>LENINE</p>
                    <p>Dois olhos negros</p>
                </div>
                    <div class="infovideos--legenda">
                    <p>LENINE</p>
                    <p>O homem dos olhos de Raio X</p>
                </div>
                <div class="infovideos--legenda">
                    <p>PATO FU</p>
                    <p>Antes que seja tarde</p>
                </div>
                <div class="infovideos--legenda">
                    <p>SANDY, JUNIOR</p>
                    <p>Imortal</p>
                </div>
            </div>
        </div>
    </div> 
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__apenasDesktop janela__invisivel"></div>    
  </div>
</div>
<!--------------------MAIN FIM-------------------------------->


<?php wp_footer(); ?>
</body>

<script type="text/javascript">
        var botao = document.getElementsByClassName("botoes--botao"),
    videos = document.getElementsByClassName("videos__wrapperVideos")[0],
    infoVideos = document.getElementsByClassName("infovideos__wrapperInfovideos")[0],
    janela = document.getElementsByClassName("janela--imagem")[0];

    function anima(x){
        var posbgjanela = '0px '+(-186*(x-1))+'px';
        var posvideos = (-376*(x-1))+'px';
        var posinfoVideos = (-200*(x-1))+'px';
        for (i=1;i<=botao.length;i++){
            botao[i-1].style.backgroundColor = 'lightgrey';
        }
        botao[x-1].style.backgroundColor = 'lightblue';
        videos.style.top = posvideos;
        infoVideos.style.top = posinfoVideos;
        janela.style.backgroundPosition = posbgjanela;
    }

    for (g=1;g<=botao.length;g++){
        let volta = g;
        botao[g-1].addEventListener("click",function(){
            anima(volta)
        });
    }
</script>


<?php get_footer(); ?>