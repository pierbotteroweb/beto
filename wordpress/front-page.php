<?php /* Template Name: home */ ?><?php get_header(); ?><!--------------------MAIN INICIO-------------------------------->

<div class="row">
  <div class="wrap">
    <div class="janela janela__imgBlurAcima"></div>
    <div class="janela janela__apenasDesktop"></div>
    <div class="janela janela__apenasDesktop"></div>
    <div class="janela janela__imgBlur"></div> 
    <div class="janela janela__menu janela__apenasDesktop">
        <a href="comerciais" class="janela__linkMenuHome">comerciais</a>
        <a href="longas" class="janela__linkMenuHome">longas</a>
        <a href="internacionais" class="janela__linkMenuHome">internacionais</a>
        <a href="clipes" class="janela__linkMenuHome">clipes</a>
        <a href="televisao" class="janela__linkMenuHome">televisão</a>
        <a href="cenarios" class="janela__linkMenuHome">cenários</a>
    </div>
    <div class="janela janela__apenasDesktop"></div>
    <div class="janela janela__imgBlurAbaixo"></div> 
    <div class="janela janela__apenasDesktop"></div>
    <div class="janela janela__apenasDesktop"></div>    
  </div>
</div>


<!--------------------MAIN FIM-------------------------------->


</body>

<script type="text/javascript">
var linkMenuHome = document.getElementsByClassName('janela__linkMenuHome'),
janelaAcima = document.getElementsByClassName('janela__imgBlurAcima')[0],
janelaMenu = document.getElementsByClassName('janela__menu')[0],
larguraBody = document.getElementsByTagName('body')[0].offsetWidth,
larguraWrap = document.getElementsByClassName("wrap")[0].offsetWidth;
console.log(larguraBody);
console.log(larguraWrap);


if(larguraBody<=larguraWrap){
    janelaAcima.innerHTML = janelaMenu.innerHTML;
}


 $(document).ready(function(){
    for (x in linkMenuHome){
    if(linkMenuHome.hasOwnProperty(x)){
        let indice = x;
        $(linkMenuHome[indice]).hover(function(){
            console.log(linkMenuHome[indice]);
            $(".janela").css("background-position","0px -"+indice*220+"px");
            $(".janela__linkMenuHome").css("background-color","#ffffff");
            $(".janela__linkMenuHome").css("color","#000000");
            $(this).css("background-color","#696969");
            $(this).css("color","#ffffff");
        });
    }
}
});
</script>    <?php get_footer(); ?>