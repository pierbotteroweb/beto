<?php /* Template Name: internacionais */ ?><?php get_header(); ?>
<div class="row">
  <div class="wrap">
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__videos">
        <div class="videos">
            <div class="videos__wrapperVideos">
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/OCeHMPM9-Ps?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>COCA-COLA LIGHT  -  O2 FILMES</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/X5IydSRMp9U?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>3 FM -   VETOR LOBO</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/MRrQomc0f5M?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>VODAFONE HE - PARADISO FILMES</p>
                </div>
                <div class="videos--item">
                    <iframe src="https://www.youtube.com/embed/kVPG_Ftl9LU?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <p>VODAFONE SOCCER - PARADISO FILMES</p>
                </div>
            </div>
        </div></div>
    <div class="janela janela__apenasDesktop janela__botoes">
        <div class="botoes">
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
            <div class="botoes--botao"></div>
        </div>
    </div>
    <div class="janela janela__apenasDesktop janela--imagem janela__imgInternacionais"></div> 
    <div class="janela janela__apenasDesktop janela__invisivel">

    </div>
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__apenasDesktop janela__infovideos">
        <div class="infovideos">
            <div class="infovideos__wrapperInfovideos">
                <div class="infovideos--legenda">
                    <p>COCA-COLA LIGHT</p>
                    <p>O2 FILMES</p>
                </div>
                <div class="infovideos--legenda">
                    <p>3 FM</p>
                    <p>VETOR LOBO</p>
                </div>
                <div class="infovideos--legenda">
                    <p>VODAFONE HE</p>
                    <p>PARADISO FILMES</p>
                </div>
                <div class="infovideos--legenda">
                    <p>VODAFONE SOCCER</p>
                    <p>PARADISO FILMES</p>
                </div>
            </div>
        </div>
    </div> 
    <div class="janela janela__apenasDesktop janela__invisivel"></div>
    <div class="janela janela__apenasDesktop janela__invisivel"></div>    
  </div>
</div>
<!--------------------MAIN FIM-------------------------------->


<?php wp_footer(); ?>
</body>


<script type="text/javascript">
        var botao = document.getElementsByClassName("botoes--botao"),
    videos = document.getElementsByClassName("videos__wrapperVideos")[0],
    infoVideos = document.getElementsByClassName("infovideos__wrapperInfovideos")[0],
    janela = document.getElementsByClassName("janela--imagem")[0];

    function anima(x){
        var posbgjanela = '0px '+(-186*(x-1))+'px';
        var posvideos = (-376*(x-1))+'px';
        var posinfoVideos = (-200*(x-1))+'px';
        for (i=1;i<=botao.length;i++){
            botao[i-1].style.backgroundColor = 'lightgrey';
        }
        botao[x-1].style.backgroundColor = 'lightblue';
        videos.style.top = posvideos;
        infoVideos.style.top = posinfoVideos;
        janela.style.backgroundPosition = posbgjanela;
    }

    for (g=1;g<=botao.length;g++){
        let volta = g;
        botao[g-1].addEventListener("click",function(){
            anima(volta)
        });
    }
</script>


<?php get_footer(); ?>